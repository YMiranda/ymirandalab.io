<h1 id="argon-design-system"><a href="https://www.creative-tim.com/product/argon-design-system">Vue Argon Design System</a></h1>

<h4 id="fully-coded-components">Fully Coded Components</h4>

<p>Vue Argon Design System is built with over 100 individual components, giving you the freedom of choosing and combining. All components can take variations in colour, that you can easily modify using SASS files.</p>

<p>You will save a lot of time going from prototyping to full-functional code, because all elements are implemented. This Design System is coming with prebuilt examples, so the development process is seamless, switching from our pages to the real website is very easy to be done.</p>

<p>Every element has multiple states for colors, styles, hover, focus, that you can easily access and use.</p>

<h4 id="complex-documentation">Complex Documentation</h4>

<p>Each element is well presented in a very complex documentation. You can read more about the idea behind this design system here. You can check the components here and the foundation here.</p>

<h4 id="example-pages">Example Pages</h4>

<p>If you want to get inspiration or just show something directly to your clients, you can jump start your development with our pre-built example pages. You will be able to quickly set up the basic structure for your web project.</p>

<h2 id="demo">Demo</h2>

<ul>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system">Index Page</a></li>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system/#/landing">Landing page</a></li>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system/#/profile">Profile Page</a></li>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system/#/login">Login Page</a></li>
  <li><a href="https://demos.creative-tim.com/vue-argon-design-system/#/register">Register Page</a></li>
</ul>

<p><a href="https://demos.creative-tim.com/argon-design-system">View More</a></p>

<h2 id="quick-start">Quick start</h2>

<ul>
  <li><a href="https://github.com/creativetimofficial/vue-argon-design-system/archive/master.zip">Download from Github</a>.</li>
  <li><a href="https://www.creative-tim.com/product/argon-design-system">Download from Creative Tim</a>.</li>
  <li>Clone the repo: <code class="highlighter-rouge">git clone https://github.com/creativetimofficial/argon-design-system.git</code>.</li>
</ul>

<h2 id="technical-support-or-questions">Technical Support or Questions</h2>

<p>If you have questions or need help integrating the product please <a href="https://www.creative-tim.com/contact-us">contact us</a> instead of opening an issue.</p>

<h2 id="licensing">Licensing</h2>

<ul>
  <li>
    <p>Copyright © 2018 Creative Tim (https://www.creative-tim.com)</p>
  </li>
  <li>
    <p>Licensed under MIT (https://github.com/creativetimofficial/argon-design-system/blob/master/LICENSE.md)</p>
  </li>
</ul>

